# README #
Contains code for automating scripts on vyom dashboard using selenium with java and rest assured.

### What is this repository for? ###

* Automating scripts on vyom dashboard using selenium with java and rest assured.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure the pom.xml is having exact version dependencies.
* Update your Vyom dashboard username and password in testData.properties
* How to run : 
    * Connect to your Vyom VPN.
    * At line 74 of src/main/java/Pages/ChangeLoadingTime.java update the file location to you system location DataFile path.
    * Enter the demand ids in DataFile.txt(It supports comma and space separated values. Use only one type(comma or space) at a time).
    * At src/main/resources/testData.properties update your VYOM dashboard username and password.
    * For first time execution: enter "mvn clean install" to download rest of the dependencies. 
    * Go to the Vyom_dashboard_automation/src/test/java/tests and right click. Select "Run Tests in tests" OR Cntrl + Shift + F10 to start the execution.
    * Check the console log for details on the execution of the demand ids.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact