package utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

@Slf4j
public class UiHelperMethods {

    private WebDriver driver ;
    private static int timeOut = 15;
    public static Configuration testData;

    public UiHelperMethods(WebDriver driver) {
        this.driver= driver;
        testData = UiHelperMethods.loadPropertiesFile("testData");
    }

    public void get(String url){
        driver.get(url);
    }

    public String getPageUrl(){
     return  driver.getCurrentUrl();
    }

    public WebElement findElement(By locator){
        try {
            return driver.findElement(locator);
        }
        catch (NoSuchElementException e){
            throw new NoSuchElementException(e.getMessage());
        }
    }


    public void sendKeys(By element, String text){
        findElement(element).sendKeys(text);
    }


    public List<WebElement> findElements(By locator){
        try {
            return driver.findElements(locator);
        }
        catch (NoSuchElementException e){
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public WebElement waitForVisibilityOf(By targetElement) {
            try {
                WebDriverWait wait = new WebDriverWait(driver, timeOut);
              return wait.until(visibilityOfElementLocated(targetElement));
            }

             catch(TimeoutException e ){
            log.info("Element is not visible: " + targetElement );
            throw new TimeoutException();
        }
    }


    public boolean waitForElementText(By targetElement, String text) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOut);
          return   wait.until(ExpectedConditions.textToBePresentInElementLocated(targetElement, text));
        } catch (TimeoutException e) {
            log.info("Element is not visible: " + targetElement);
            throw new TimeoutException();
        }
    }



    public void waitForClickableOf(By targetElement) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOut);
            wait.until(ExpectedConditions.elementToBeClickable(targetElement));
        }
        catch(TimeoutException e ){
            log.info("Element is not clickable: " + targetElement );
            throw new TimeoutException();

        }
    }

    public void keypressEventWithAction(String key){
        Actions actions = new Actions(driver);
        actions.sendKeys(key);
    }

    public void clickOnElementUsingActions(By element){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(element));
        actions.click().perform();
    }


    public void clickOnElementWithJavascript(By element){
        waitForVisibilityOf(element);
        JavascriptExecutor js= (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", findElement(element));
    }

    public void click(By element){
        findElement(element).click();
    }

    public void doubleClick(By element){
        Actions actions = new Actions(driver);
        WebElement elementLocator = findElement(element);
        actions.doubleClick(elementLocator).perform();
    }


    public void takeScreenshot(){
        try
        {
            TakesScreenshot ts=(TakesScreenshot)(driver);
            File source=ts.getScreenshotAs(OutputType.FILE);

            FileUtils.copyFile(source, new File(".//failureScreenshots//"+ "Screenshot_" +
                    new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss")
                            .format(new GregorianCalendar().getTime()) + ".png"));
            log.info("Scenario failed and screenshot saved in failureScreenshots folder");
        }
        catch (WebDriverException | IOException e)
        {
            log.error("Exception while taking screenshot "+e.getMessage());
        }
    }


    public boolean isElementExists(By targetElement) {
        try {
            waitForPageLoad();
           return !driver.findElements(targetElement).isEmpty();
        }
        catch(NoSuchElementException e ){
            log.info("Element is not present: " + targetElement );
            try {
                throw new NoSuchMethodException();
            } catch (NoSuchMethodException e1) {
                e1.printStackTrace();
            }

        }
        return false;
    }

    public String getAttributeAriaLabel(By element){
        return findElement(element).getAttribute("aria-label");
    }



    public void waitForPageLoad() {
        new WebDriverWait(driver, timeOut).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }


    public static void saveTestData(String filename, String key, String value) {
        File file = new File("src/main/resources/"+filename + ".properties");

        PropertiesConfiguration config = new PropertiesConfiguration();
        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();
        try {
            try {
                config.setLayout(layout);
                layout.load(config,new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        config.setProperty(key, value);
        try {
            layout.save(config,new FileWriter(file,false));
        } catch (ConfigurationException| IOException e) {
            e.printStackTrace();
        }
    }

    public static Configuration loadPropertiesFile(String filename) {
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                        .configure(params.properties().setBasePath("src/resources")
                                .setFileName(filename + ".properties")
                        .setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
        try {
            return builder.getConfiguration();
        } catch (ConfigurationException cex) {
            // loading of the configuration file failed
            log.info(filename + " is not loaded" + cex.getMessage());
            return null;
        }

    }

    public void setCookie(String user){
        Cookie cookie = new Cookie.Builder(testData.getString("tokenKey"),testData.getString(user))
               // .expiresOn(new Date(System.currentTimeMillis() + 600000))
                .isHttpOnly(true)
                .isSecure(false)
                .build();

        driver.manage().addCookie(cookie);
    }

    public String getSessionCookie(){
        String value= driver.manage().getCookieNamed(testData.getString("tokenKey")).getValue();
        log.info("cookie: " + value);
        return value;
    }

    public void deleteCookie(String cookieName){
        driver.manage().deleteCookieNamed(cookieName);
    }


    public void loginViaSetCookie(){
        get(UiHelperMethods.testData.getString("baseUrl"));
        waitForPageLoad();
        setCookie("adminUserCookie");

    }





}
