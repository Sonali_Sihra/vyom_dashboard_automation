package utils.webDriverFactory;

// custom exception class to throw exception if browser is not supported
public class BrowserNotSupported extends Exception {

    BrowserNotSupported(String message) {
        super(message);
    }
}
