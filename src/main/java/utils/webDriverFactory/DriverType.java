package utils.webDriverFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum DriverType {
    CHROME,
    FIREFOX;


    public static DriverType getBrowserName() {
        DriverType driverType = DriverType.CHROME;
        if (System.getProperty("browser") != null) {
            try {
                driverType = DriverType.checkIfBrowserIsSupported(System.getProperty("browser"));
            }
            catch (BrowserNotSupported bex){
                log.error( "tests will run on " + driverType + " by default");
            }
        }
        return driverType;
    }

    public static DriverType checkIfBrowserIsSupported(String browserName) throws BrowserNotSupported {

        for (DriverType browser : DriverType.values()) {
            if (browser.name().equalsIgnoreCase(browserName)) {
                return browser;
            }
        }
        throw new BrowserNotSupported("Browser is not supported. Supported browsers are:" + java.util.Arrays.asList(DriverType.values()));
    }

    }