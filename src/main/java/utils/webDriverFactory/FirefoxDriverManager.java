package utils.webDriverFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends BaseDriverManager {

    @Override
     void createDriver() {
        WebDriverManager.firefoxdriver().setup();
        driverInstance = (new FirefoxDriver());
        getWebDriver().manage().window().maximize();
    }
}
