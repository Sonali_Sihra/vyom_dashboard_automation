package utils.webDriverFactory;

import org.openqa.selenium.WebDriver;

public abstract class BaseDriverManager {

      WebDriver driverInstance ;

    abstract void createDriver();

    public synchronized void quitDriver() {
        if(getWebDriver() !=null) {
            getWebDriver().quit();
        }
    }

    public WebDriver getWebDriver() {

        return driverInstance;
    }
}
