package utils.webDriverFactory;



public class DriverManager {



    public static BaseDriverManager createDriverInstance() {
        BaseDriverManager baseDriverManager;
        DriverType browserName = DriverType.getBrowserName();

        switch (browserName) {
            case CHROME:
                baseDriverManager = new ChromeDriverManager();
                baseDriverManager.createDriver();
                break;
            case FIREFOX:
                baseDriverManager = new FirefoxDriverManager();
                baseDriverManager.createDriver();
                break;
            default:
                baseDriverManager = new ChromeDriverManager();
                baseDriverManager.createDriver();
        }
        return baseDriverManager;
    }
}
