package utils;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import groovy.json.JsonException;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

@Slf4j
public class RestUtil {

	private  Map<String, String> getRequestHeaders;
	private  Map<String, String> getRequestParameters;
	private HashMap<String,String> postRequestHeaders;
	private HashMap<String,String> postRequestParameters;
	RequestSpecification baseSpec;




	public RestUtil(RequestSpecification baseSpec){
        this.baseSpec = baseSpec;
    }

	/**
	 * common method to initiate the GET request
	 * @param getPath : url of the request
	 * @param values: parameters needs to be send in the request
	 * @return JSON response of the request
	 */
	public Response getRequest(String getPath, Map<String, String> values, Map<String, String> headers)
	{
		Response jsonResponse = null;
	   try {
                jsonResponse =
                        given()
                                .spec(baseSpec)
                                .parameters(values)
                                .headers(headers)
                                .when()
                                .get(getPath);

		}
		catch(Exception e){
			log.error("Exception occured:" + e.getMessage());
		}


		log.info("getRequest response: " + jsonResponse.asString());
		return jsonResponse;

	}


	public Response postJSONRequest(String getPath, String body, Map<String, String> headers)
	{
		Response jsonResponse = null;

		try {

				jsonResponse =
						given()
								.spec(baseSpec)
								.contentType("application/json")
								.headers(headers)
								.body(body)
								.when()
								.post(getPath);
		}
		catch(JsonException e){
			log.error("Exception occured:" + e.getMessage());
		}
		log.info("post JSON response: " + jsonResponse.asString());
		return jsonResponse;

	}




	public Response delete(String path,Map<String, String> params, Map<String, String> headers) {

		return RestAssured
				.given()
				.spec(baseSpec)
				.headers(headers)
				.delete(path, params);
	}

}
