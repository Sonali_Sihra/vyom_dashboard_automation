package Pages;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import utils.UiHelperMethods;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;

public class ChangeLoadingTime extends UiHelperMethods
{
    private WebDriver driver;

    public ChangeLoadingTime(WebDriver driver) {
        super(driver);
        this.driver = driver;
        driver.manage().window().maximize();
    }
    private final By usernameTextField = By.xpath("//div[@class=\"container\"]//form[@class=\"form-signin\"]//input[@placeholder=\"username\"]");
    private final By passwordTextField = By.xpath("//div[@class=\"container\"]//form[@class=\"form-signin\"]//input[@placeholder=\"password\"]");
    private final By loginButton = By.xpath("//div[@class=\"container\"]//input[@class=\"btn btn-lg btn-primary btn-block\"]");
    private final By DropDown = By.xpath("//span[@class=\"select2-arrow ui-select-toggle\"]");
    private final By Script = By.xpath("//div[@class=\"select2-result-label ui-select-choices-row-inner\"]//div[text()=\"ChangeLoadingTime.sh\"]");
    private final By Timestamp = By.xpath("/html/body/div/container/form/div[2]/div/input");

    public void enterUsername(String username){
        waitForVisibilityOf(usernameTextField);
        sendKeys(usernameTextField,username);
    }

    public void enterPassword(String password){
        waitForVisibilityOf(passwordTextField);
        sendKeys(passwordTextField,password);
    }

    public void clickOnLoginButton(){
        waitForClickableOf(loginButton);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(loginButton);
    }

    public void OpenDropDown(){
        waitForClickableOf(DropDown);
        click(DropDown);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(Script);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void TypeInField() throws IOException {
        WebElement element = driver.findElement(By.xpath("/html/body/div/container/form/div[1]/div/input"));
        element.clear();
        BufferedReader in = new BufferedReader(new FileReader("/home/sonali/Vyom_dashboard_automation/src/test/DataFile"));
        String line;
        String[] tokens = new String[0];
        while ((line = in.readLine()) != null) {
            tokens = line.split("\\s+|,\\s*|\\.\\s*");
//            System.out.println(line);
            System.out.println("Processing Demands:" + Arrays.toString(tokens));
        }
        in.close();
        for (String c : tokens) {
            element.sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT, Keys.END),c);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long currentTime = timestamp.getTime();
            WebElement element1 = driver.findElement(By.xpath("/html/body/div/container/form/div[2]/div/input"));
            element1.sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT, Keys.END));
            sendKeys(Timestamp, String.valueOf(currentTime));
            System.out.println("Demands " + c + " are extended till " + currentTime);
            click(By.xpath("//div[@class=\"col-sm-offset-4 col-sm-4\"]//button"));
            driver.switchTo().alert().accept();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String res = driver.findElement(By.xpath("//p[contains(@class,\"alert-warning\")]")).getText();
            System.out.println("Response: " + res);
        }
    }
    @Test
    public void Cache() {
        RestAssured.baseURI="https://supply.vyom.com/api";
        RequestSpecification httpRequest = RestAssured.given();
//        JSONObject requestParams = new JSONObject();
        httpRequest.header("Content-Type","application/json");
        httpRequest.header("Cache-Control","no-cache");
        httpRequest.header("Connection","keep-alive");
        httpRequest.header("Host","supply.vyom.com");
        httpRequest.header("Postman-Token","supply.vyom.comb7ad07c1-269e-49fe-8494-a51626f20d65,44e99a61-90ec-42ab-b04b-419ddde6eb54,5a55096c-40bc-4e87-8ffc-1d2a75b14bd6");
        httpRequest.header("cache-control","no-cache,no-cache");
        httpRequest.header("User-Agent","PostmanRuntime/7.20.1");
        httpRequest.header("ap","g3310c0dfvcvw91ebv93");
        httpRequest.header("au","fos-user");
        httpRequest.header("s","APP");
        httpRequest.header("v","1");
        Response response = httpRequest.request(Method.POST,"/util/admin/clearcache?value=demandCache");

        //print response on console
        String responseBody = response.getBody().asString();
        System.out.println("Cache response is: " +responseBody);
        int statusCode = response.getStatusCode();
        System.out.println("Status code: "+statusCode);
//        Assert.assertEquals(statusCode, "200");
//        String successCode = response.jsonPath().get("SuccessCode");
    }

    public MainPage login(String username, String password) throws IOException {
        enterUsername(username);
        enterPassword(password);
        clickOnLoginButton();
        OpenDropDown();
        TypeInField();
        Cache();
        return new MainPage(driver);
    }
}