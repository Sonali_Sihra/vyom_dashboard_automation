package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.UiHelperMethods;

public class MainPage extends UiHelperMethods {

    private WebDriver driver;

    private By profileOptions = By.xpath("//div[@class=\"ng-scope\"]//h2[text()=\"Welcome to Support\"]");


    public MainPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isProfileOptionVisible(){
        return waitForVisibilityOf(profileOptions).isDisplayed();
    }
}
