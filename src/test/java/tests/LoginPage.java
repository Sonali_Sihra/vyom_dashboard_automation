package tests;

import Pages.ChangeLoadingTime;
import Pages.MainPage;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.UiHelperMethods;
import utils.webDriverFactory.BaseDriverManager;
import utils.webDriverFactory.DriverManager;

import java.io.IOException;

public class LoginPage {



    BaseDriverManager baseDriverManagerInstance;
    ChangeLoadingTime changeLoadingTimePage;
    MainPage helpPage;
    UiHelperMethods uiHelperMethods;

    @BeforeMethod
    public void setUp() {
        baseDriverManagerInstance= DriverManager.createDriverInstance();
        uiHelperMethods = new UiHelperMethods(baseDriverManagerInstance.getWebDriver());
        changeLoadingTimePage = new ChangeLoadingTime(baseDriverManagerInstance.getWebDriver());

    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result){
        if (result.getStatus() == ITestResult.FAILURE) {
            uiHelperMethods.takeScreenshot();
        }
        baseDriverManagerInstance.quitDriver();
    }

    @Test
    public void verifyLogin() throws IOException {
        changeLoadingTimePage.get(UiHelperMethods.testData.getString("baseUrl"));
        helpPage = changeLoadingTimePage.login(UiHelperMethods.testData.getString("username"),UiHelperMethods.testData.getString("password"));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
