package tests;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utils.UiHelperMethods;

public class TestRunnerUtil {

@BeforeSuite
public void oneTimeSetup(){
    UiHelperMethods.loadPropertiesFile("testData");
}

@AfterSuite(alwaysRun = true)
public void browserCleanUp(){
//     baseDriverManagerInstance.quitDriver();
}




}
